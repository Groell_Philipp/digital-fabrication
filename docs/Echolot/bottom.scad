module bottomPart() {
	difference() {
		union() {
			color("black") square([130,70], center=false);
			translate([45,-5]) color("black") square([30,5], center=false);
			translate([45,70]) color("black") square([30,5], center=false);
			translate([-5,20]) color("black") square([5,30], center=false);
		}
		translate([40,32.5]) color("white") square([50,5], center=false);
	}
}

module handle() {
	difference() {
		color("black") circle(d=140, $fn=70);
		color("white") circle(d=120, $fn=70);
		translate([-70,0]) color("white") square([140,70], center=false);
	}
	translate([-70,0]) color("black") square([140,10], center=false);
	translate([-25,10]) color("black") square([50,5], center=false);
}

bottomPart();
translate([0,-16]) handle();