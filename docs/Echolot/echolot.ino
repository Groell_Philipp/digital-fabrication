/*
 * Software for the "Echolot"
 * @author Philipp Gröll
 * https://groell_philipp.gitlab.io/digital-fabrication/
 * @version 1.0
*/

// Includes
#include <Servo.h>

// Global variables and objects
byte vibrationMotorPin = 5;
byte servoMotorPin = 9;
byte triggerPin1 = 7;
byte echoPin1 = 3;
byte triggerPin3 = 12;
byte echoPin3 = 11;
byte triggerPin2 = 2;
byte echoPin2 = 6;
int steps = 100;
int maxDistance = 3000;
int minDistance = 40;
float distance = 0.0;
int currentPosition = 90;
Servo s;

// Global functions

/**
	* returns the distance in cm measured by a given sensor
	* A sensor is specified by its trigger and echo pin
**/
float getDistance(byte trigger, byte echo) {
  float dist = 0.00;
  long tm = 0; // stores the time a signal takes to reach an object and return
  digitalWrite(trigger, LOW);
  delayMicroseconds(3);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10); // a 10 microseconds HIGH signal triggers the sensor
  digitalWrite(trigger, LOW);
  delayMicroseconds(10);
  tm = pulseIn(echo, HIGH); // the time measured by pulseIn = 2 * distance to the object recognised
  dist = tm * 0.034 / 2; // convert to cm and save the distance only once
  return(dist);
}

/**
	* Makes the device vibrate at the maximum intensity set in steps for half a second
**/
void statusFeedback() {
  analogWrite(vibrationMotorPin, steps);
  delay(500);
  analogWrite(vibrationMotorPin, 0);
}

/**
	* Inicial pin configuration
**/
void setup() {
  pinMode(vibrationMotorPin, OUTPUT);
  pinMode(triggerPin1, OUTPUT);
  pinMode(echoPin1, INPUT);
  pinMode(triggerPin2, OUTPUT);
  pinMode(echoPin2, INPUT);
  pinMode(triggerPin3, OUTPUT);
  pinMode(echoPin3, INPUT);
  s.attach(servoMotorPin); // Make the servo motor available for the servo object
  s.write(currentPosition); //calibrating the servo
  Serial.begin(9600); // Establish a serial connection to transfer debugging information
}

/**
	* Request the distance from each sensor, move the servo in the direction of the closest object and set the vibration intensity
**/
void loop() {
  
  // Request the distance from each sensor
  float distance1 = getDistance(triggerPin1, echoPin1);
  float distance2 = getDistance(triggerPin2, echoPin2);
  float distance3 = getDistance(triggerPin3, echoPin3);
  
  // rotation
  if ((distance1 < distance2) && (distance1 < distance3) && (distance1 <= maxDistance)) { // the object in front of the device is the closest one and not outside of the range in which the sensor is supposed to operate
    s.write(90);
  }
  else if ((distance2 < distance1) && (distance2 < distance3) && (distance2 <= maxDistance)) { // The object on the left is the closest one
    s.write(0);
  }
  else if (distance3 <= maxDistance) { // now, the closest object is definitely on the right
    s.write(180);
  }
  
  // vibration
  if (distance1 <= distance2) {
    distance = distance1; // distance stores the distance to the closest object
  }
  else if (distance2 < distance1) {
    distance = distance2;
  }
  if (distance3 < distance) {
    distance = distance3;
  }
  Serial.println(distance);
  int intensity = map(distance, minDistance, maxDistance, steps, 0); // Maps the distance to the closest object from the range defined by minDistance and maxDistance to its corresponding vibration intensity
  analogWrite(vibrationMotorPin, intensity); // The intensity defines the on time of the PWM signal
} 
