deviceWidth = 140;
deviceDepth = 80;
deviceHeight = 50;
materialThicknes = 5;
sensorDiameter = 16;
buttonDiameter = 16;

difference() {
	color("black") cube([deviceWidth,deviceDepth,deviceHeight], center=false);
	
	// top sensor
	translate([(2*materialThicknes+10),((deviceDepth/2)-(sensorDiameter/2)-5),0]) color("white") cylinder(h=100,d=sensorDiameter,center=false);
	translate([(2*materialThicknes+10),((deviceDepth/2)+(sensorDiameter/2)+5),0]) color("white") cylinder(h=100,d=sensorDiameter,center=false);
	
	// left and right sensors
	translate([(materialThicknes+(sensorDiameter/2)+1.5),0,(deviceHeight-(2*materialThicknes)-10)]) rotate([-90,0,0]) color("white") cylinder(h=100,d=sensorDiameter,center=false);
	translate([(materialThicknes+sensorDiameter+(sensorDiameter/2)+11.5),0,(deviceHeight-(2*materialThicknes)-10)]) rotate([-90,0,0]) color("white") cylinder(h=100,d=sensorDiameter,center=false);
	
	// button
	translate([(deviceWidth-materialThicknes-(buttonDiameter/2)-3),(deviceDepth/2),0]) color("white") cylinder(h=100,d=buttonDiameter,center=false);
	
	// servo
	translate([0,((deviceDepth-23)/2),materialThicknes*2]) color("white") cube([30,23,12], center=false);
	
	// battery connector
	translate([deviceWidth-2.5,((deviceDepth-30)/2),(deviceHeight-materialThicknes-13)]) color("white") cube([2.5,30,13], center=false);
	translate([(deviceWidth-2*materialThicknes),((deviceDepth-30)/2)+32,(deviceHeight-materialThicknes-6.5)]) rotate([0,-90,0]) color("white") cylinder(h=100,d=4,center=false);
	
	// USB connector
	translate([deviceWidth-materialThicknes,(deviceDepth-13)/2,materialThicknes]) color("white") cube([10,13,12], center=false);
	
	// remove the inside
	translate([materialThicknes,materialThicknes,0]) color("white") cube([deviceWidth-(2*materialThicknes),deviceDepth-(2*materialThicknes),(deviceHeight-materialThicknes)], center=false);
	
	// joints
	translate([0,(deviceDepth-30)/2,0]) color("white") cube([materialThicknes,30,materialThicknes], center=false);
	translate([(2*materialThicknes+45),0,0]) color("white") cube([30,100,materialThicknes], center=false);
}