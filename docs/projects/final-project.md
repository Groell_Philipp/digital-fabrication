# Final Project 

This is the documentation for my final project of the fundamentals of digital fabrication course at the Rhine-Waal university of applied sciences in the winter term 2019/2020.
After explaining the idea behind the project and the development approach, each part of the project, starting from the design of the casing and moving over the electronics to the software, will be discussed. 
Internal ressources for further ilustration and for the replecation of the project, as well as external ressources will be provided at the end. 
Before going further into details, an important security advice must be given: do not use the device or parts of it, namingly the ultra sonar sensors, around animals, especially dogs. Since they are able to perceive the frequencies, they are going to cause iritations and therefore unpredictable reactions. 

## Project idea and approach in development 

My final project is called "Echolot". It is a device which assists blind people to navigate through outdoor surroundings independently. 
Using it either as a hand-held gadget or attaching it to clothes, it is meaned to detect objects which are outside the range of a white cane. 
Objects are detected by three ultra sonar sensors which are placed in a 90° angle on the top and on the left and the right side of the device. The distance to the closest object is displayed by vibration. The closer an object is the more the intensity of the vibration is going to increase. The direction in which the closest object was detected is displayed by a touchable servo motor on the top of the divece. 
The device is powered by a 9 V battery and is turned on and off by a simple push button. 
The first step was to design a basic user experience. With respect to the very limited amound of time I was only able to utilise my personal experience. 
Therefore, I defined the following three requirements: 

1. The user must be able to differenciate between the information about where an object is located and how far it is
2. At any time, the user must be able to perceive the environment around him/her propperly
3. All elements of interaction must be non-visual

The second step was to collect the hardware components followed by designing the software. This resulted in a list of functions to be implemented including their parameters and return types. 
In order to be able to design a correct casing considering the measurements of the hardware components, I first assembled them and implemented the software. 
The last step was to arrange the electronics inside the casing and adjust the maximum intensity of the vibration motor to the total weight of the device. 
The photo below showes the complete Echolot: 
![The Echolot laying on a table](../images/final-project/Echolot.jpg) 

## Design of the casing 

The casing consists of a 3D-printed box with holes for the sensors, the servo motor, the button and the wire of the battery connector to go through. Furthermore, it has a deepening for the battery connector and a hole for the Mini USB port of the Arduino.
The bottom side is left open with protusions for the joints of the removable bottom part. 
The box itself is created by two cube objects which are subtracted from each other. The holes are cut by cylinder and cube objects placed on top of the surfaces and going through the cubes which are also subtracted. Unfortunately, it was not possible to position the Arduino inside the box in a way that it fits to the hole for the Mini USB port. The image showes the 3D preview of the box in OpenSCAD: 
![A screenshot of OpenSCAD which showes the right and front side of the box in the 3D preview](../images/final-project/screenshot-box.jpg) 
In order for the box to be stable, in terms of the design it is important to leave a wall which is thick enough when subtracting the cubes. I chose a thickness of 0.5 cm. In terms of printing it is recommended to work with a high infill. 
The bottom part consists of two laser cut parts which are joined together. The actual bottom is made of a big square which fits exactly to the measurements of the opening of the box and three squares on the sides which are used as joints. In the middle, a small square is subtracted which is used as protusion for the handle. 
The handle is a quite complex object. Two semicircles are subtracted from each other and the open side of the semicircles is added to a square to which an other smaller square is added as a joint.
A semicircle can be created by creating a full circle and subtracting a square with the diameter of the full circle from the middle of the full circle. The image illustrates the handle and bottom part: 
![A screenshot of OpenSCAD which showes the handle and the bottom part in the 2D preview](../images/final-project/screenshot-bottom-handle.jpg) 
Because I chose a wall thickness of 0.5 cm before, the thickness of the material which is used for cutting the bottom part also has to be 0.5 cm.

## Electronics 

The following components are required to build the electronics part of the project: 

- An Arduino Uno
- Three ultra sonar distance sensors HC-SR 04
- A vibration motor
- A servo motor
- A 9 V battery
- a battery connector
- A push button
- A mosfet

### Assembling the components 

Starting from the battery connector, the positive wire is connected to one pin of the button and the GND is connected to the GND of the Arduino. The other pin of the button is connected to the VIn. 
The sonar sensors are equipped with four pins: 5V, Trigger, Echo and GND. The 5Vs are connected to the board while the GNDs are connected to the board's GND. The trigger and echo pins require i/o pins. For both  digital pins can be used. 
The vibration motor requires the mosfet to be powered. A mosfet has three pins: basis (B), emitter (E) and collector (C). B is connected to the board, E is connected to the board's GND and C goes to the vibration motor. 
The vibration motor itself is equipped with two pins. One is connected to the transistor's C pin and the other one is connected to the VCC. This connection requires PWM functionality. 
The servo motor has three pins: 5V, GND and signal. While 5V and GND are connected to the board and the board's GND, the signal pin is connected to an analog pin. PWM functionallity is not necessary, because the functionality is taken over by the servo library. 
Below, you can see a photo of the electronics: 
![An Arduino Uno with the components connected to it laying on a table](../images/final-project/electronics.jpg)

## Software 

The software can be devided into five parts: 

- Includes
- Global variables and objects
- Global functions
- The setup function
- The loop function 

### Includes 

In order to be able to control the servo motor the built in servo library is required. By using the ``` #include servo.h ``` command the functions provided in the servo library are accessible in the current sketch. 

### Global variables and objects 

The following variables and objects are used throughout the entire program: 

<dl>
    <dt>byte vibrationMotorPin</dt> <dd>the pin at which the vibration motor is connected to the Arduino</dd>
    <dt>byte servoMotorPin</dt> <dd>the pin at which the servo is connected to the Arduino</dd>
    <dt>byte triggerPin1</dt> <dd>the pin at which the trigger of the first sonar sensor is connected to the Arduino</dd>
    <dt>byte echoPin1</dt> <dd>the pin at which the echo of the first sonar sensor is connected to the Arduino</dd>
    <dt>byte triggerPin2</dt> <dd>the pin at which the trigger of the second sonar sensor is connected to the Arduino</dd>
    <dt>byte echoPin2</dt> <dd>the pin at which the echo of the second sonar sensor is connected to the Arduino</dd>
    <dt>byte triggerPin3</dt> <dd>the pin at which the trigger of the third sonar sensor is connected to the Arduino</dd>
    <dt>byte echoPin3</dt> <dd>the pin at which the echo of the third sonar sensor is connected to the Arduino</dd>
    <dt>int steps</dt> <dd>defines the number of levels of the intensity of the vibration</dd>
    <dt>int maxDistance</dt> <dd>defines the maximum distance of an object to which the intensity of the vibration decreases</dd>
    <dt>int minDistance</dt> <dd>defines the minimum distance of an object to which the intensity of an object increases</dd>
    <dt>float distance</dt> <dd>stores the distance to the closest object</dd>
    <dt>int currentPosition</dt> <dd>stores the position the servo has to move to in degrees relative to the servo's origin</dd>
    <dt>Servo s</dt> <dd>inicialisation of a servo object which is required to control the servo motor</dd>
</dl> 

### Global functions 

One action is represented by a repetative code pattern and therefore is split up in a separate function: getDistance. 

#### getDistance 

- parameters: trigger and echo pin of the sensor from which the distance is requested
- returns: a float which contains the distance in centimeters 

The getDistance function returns the distance measured by one of the ultra sonar sensors which is specified by its trigger and echo pin. 
The sensor is triggered by a 10 microseconds HIGH signal which is sent by using the digitalWrite function on the trigger pin. 
By using the pulseIn function on the echo pin the time the signal takes to reach an object and to return is calculated. The pulseIn function takes a value (LOW|HIGH) and starts a timer until the signal goes to the opposit and back to the value specified in the function parameter. 
To convert this value to centimeters it has to be multiplied with 0.034 and finally divided by 2 to get only one way. This is the value returned by the function. 
To give an example how Arduino code looks like in general, you can see the whole getDistance function below: 

``` 
float getDistance(byte trigger, byte echo) {
  float dist = 0.00;
  long tm = 0; // stores the time a signal takes to reach an object and return
  digitalWrite(trigger, LOW);
  delayMicroseconds(3);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10); // a 10 microseconds HIGH signal triggers the sensor
  digitalWrite(trigger, LOW);
  delayMicroseconds(10);
  tm = pulseIn(echo, HIGH); // the time measured by pulseIn = 2 * distance to the object recognised
  dist = tm * 0.034 / 2; // convert to cm and save the distance only once
  return(dist);
} 
``` 

### The setup function 

The setup function is run only once when the Arduino has booted. 
In order to be able to work with pins, their mode has to be specified first. It can either be INPUT or OUTPUT. This is usually done in the setup function by calling the pinMode function. 
Since the vibration motor pin and the sensors' trigger pins are used for sending signals, they are output pins. The servo motor pin is an output pin as well, but in order to work with it the attach function from the servo library has to be called. It attaches a pin given as parameter to an inicialised servo object. 
The sensors' echo pins are input pins, because their function is to transfer incoming signals. 
In addition to setting the pin modes, the servo motor has to be calibrated. 
Because rotating by a negative angle is not possible but actually necessary, the servo's inicial position has to be at 90° from its origin. The servo is rotated by using the write function from the servo library which takes an absolute position between 0 and 180 as its parameter. 

### The loop function 

The loop function keeps on running as long as the Arduino is powered. 
At first, the distance from each sensor is requested. Then, the servo moves in the direction of the closest object and the intensity of the vibration is set according to the distance. Distance1 is the distance measured by the sensor at the front, distance2 is the distance from the sensor on the right and distance3 is the distance from the sensor on the left. 
The pattern for moving the servo and adjusting the vibration is quite similar. Before performing the action, the closest object has to be determined. This is done by comparing the three distances in the if statements. 
In both cases it has to be insured that the distances are within the range set by the variables minDistance and maxDistance. In the case of moving the servo this is done by requiring the distance to the closest object to be less than or equal to maxDistance. 
In terms of the vibration, this is already insured by the function which is used to set the intensity of the vibration. The eassiest way to do this is to use the map function. It maps a given value from one scale to another scale. 
In this case, the value which has to be mapped is the distance to the closest object. The scales are (minDistance, maxDistance) and (steps, 0). 
The result of this is passed on to the analogWrite function on the vibration motor pin. It uses PWM functionallity. Therefore, the value passed on to this function defines how much time the signal spends on.

## Video 

[First demo of the Echolot prototype at Youtube](https://youtu.be/sAdivlrEctU)

## Files 

- [Echolot software (Arduino source code file)](../Echolot/echolot.ino)
- [Echolot box design (OpenSCAD file)](../Echolot/box.scad)
- [Echolot box design (STL 3D object)](../Echolot/box.stl)
- [Echolot bottom part design (OpenSCAD file)](../Echolot/bottom.scad)
- [Echolot bottom part design (DXF file)](../Echolot/bottom.dxf)

## External links 

- [Bipolar transistor tutorial, the BJT transistor (in German)](https://www.electronics-tutorials.ws/de/transistoren/der-bipolartransistor.html)
- [Distance measuring with Arduino using the sonar sensor HC-SR 04 (in German)](https://www.mikrocontroller-elektronik.de/ultraschallsensor-hc-sr04/)
- [OpenSCAD Cheatsheet](http://www.openscad.org/cheatsheet/index.html)
- [OpenSCAD language documentation](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language)
- [pulseIn() Arduino language reference](https://www.arduino.cc/reference/en/language/functions/advanced-io/pulsein/)
- [Servo library Arduino language reference](https://www.arduino.cc/en/Reference/Servo)
