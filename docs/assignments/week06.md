# 6. Electronics design 

This week I started to design a PCB board for my final project. Below you will find the parts that are needed and the way they are connected on the board. 

## Parts 

- an ATMega328 board
- a 7 or 9 V battery
- a 5 V step down voltage regulator
- an on/off push button
- a BXD53C transistor
- a vibration motor
- 3 HC-SR 04 sonar distance sensors 

## Connecting the parts 

Starting from the battery, the positive wire has to be connected to one pin of the button. Another pin of the button is connected to the voltage regulator and the battery's GND goes to the input of the voltage regulator. 
The output of the voltage regulator has to be connected to the VCCs of the sensors, the vibration motor and the board.
The GND is connected to the GND of the board. 
The sonar distance sensors are equipped with 4 pins: 5V, Trigger, Echo and GND. The 5Vs are connected to the output of the voltage regulator and the GNDs have to be connected to the board's GND. 
For the Trigger and Echo pins i/o pins have to be used. Both have to be digital pins. 
The vibration motor requires a transistor to be powered. It has 3 pins: Basis (B), Emitter (E) and Collector (C).
B is connected to the board, E is connected to the board's GND and C goes to the vibration motor. 
The vibration motor itself is equipped with 2 pins. One is connected to the transistor's C pin and the other one is connected to the board's GND.
This connection requires PWM functionality. Therefore, a pin of Port D has to be used. 

## Usefull links 

- [Distance measuring with Arduino using the sonar sensor HC-SR 04 (in German)](https://www.mikrocontroller-elektronik.de/ultraschallsensor-hc-sr04/)
- [Bipolar transistor tutorial, the BJT transistor (in German)](https://www.electronics-tutorials.ws/de/transistoren/der-bipolartransistor.html)