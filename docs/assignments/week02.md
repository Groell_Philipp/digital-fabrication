# 2. Computer Aided Design

This week I learned how to use the Computer Aided Design (CAD) tool "OpenSCAD" to create 2D and 3D objects.

In contrast to other popular CAD software, OpenSCAD uses a scripting language to define objects, perform rotations etc. 

## 2D modelling

The 2D object I designed is the result of experimenting with the essencial 2D components: squares, circles and text. It is a 2D model of the front side of my final project.

The main componant is a black square. On the bottom, centered on the x-axis, a yellow circle is placed, which will later be an on / off button. Another small black circle centered in the bigger circle is supposed to be a marker on the button.

On the top of the square, the text "I am a 2d model" is written in Braille. Obviously, for my final project, this text will be replaced. 

### Positioning objects 

In OpenSCAD, objects are positioned by applying translations and rotations to them. The following example creates a circle which has its focus at (3|1.5):

```
translate([30,15]) color("yellow") circle(r=7.5);
```

First, a translation of 30 mm on the x-axis and 15 mm on the y-axis is performed. Then, the circle object is inicialised.

## 3D modelling

Instead of transforming my 2D object to 3D, I chose to design a tray for an external PC keyboard.

It consists of the surface where the keyboard is put on and six "feet". In order to be able to export each (2D) part separately, each type of part (the main part and two kinds of feet) are grouped in so called modules which can be reused within the code.

The 3D model is created by joining the 2D models. In order to make it easyer to join the parts after cutting them, I implemented a system of small squares which are cut into the parts to be able to stick them together.

The surface module (mainPart) is designed to have a fixed position. The feet (module square1 and module square2) accept parameters for their translation and rotation when inicialising them. 

## Files

- [2D object *.scad file](../files/openscad/square.scad)
- [3D object *.scad file](../files/openscad/keyboard_tray.scad)

## External resources

- [OpenSCAD language documentation](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language)
- [OpenSCAD Cheatsheet](http://www.openscad.org/cheatsheet/index.html)