# 1. Project Management

This week I set up my project website by learning the Markdown language and making myself familiar with the version control system GitLab.

## Markdown

Markdown is a language to structure documents using inline commands for headings, hyperlinks, list items etc.
For this assignment, I edited three files:

- the index file
- the `About me` page
- and what you are reading right no

### Code snippets

Here are some code examples to illustrate how Markdown works.

- ``` # Home ``` - marks "Home" as a heading level one
- ``` [Go back to home](../index.md) ``` - creates a hyperlink to the index.md file which is labeled with "Go back to home"
- ``` - item 1 ``` - creates a list item called "item 1"

## GitLab

GitLab is a version control system based on a software called Git. Simplyfied, this means that you always have access to previous versions of your files and can manage them.

Files are stored in so called repositories. I did this using Gitlab's web interface but it also can be done using a terminal.

In order to work with Gitlab without the web interface, I first had to install the Git client for Windows, which emulates a unix terminal.

The last step to get access to my repository was to clone it to my computer using the command "git clone " followed by a cloning URL which can be copied from the web interface if you click "Clone" on your repository page.

Once I entered the credentials for my account, I was done for this step. But however, always entering user name and password is not very convenient so I decided to use the Secure Shell Protocol (SSH) instead.

Therefore, I first generated a key pair on my computer. Then I had to add the public key to my Gitlab account. This can be done on the Gitlab website by clicking on your profile photo -> settings -> SSH keys.

After saving the public key to my account I had to clone the repository again using a clone URL specially designed for connections via SSH.  

### Frequently used actions

The actions which I use to manage my files are:

- add
- commit
- push

#### Adding files

Changed files are not noticed automatically. Therefore, you have to tell Git which files you want to upload.

This is done by the command "git add". Since I do not want to specify them all the time I simply add the parameter "--all".

#### Making a commit

A commit marks a certain point in your file history.

The files added previously are grouped in a commit by using the command "git commit -m 'commit message'". Commit messages are essencial for you to keep track of your file history.

#### Pushing files

Once a commit is created, this commit can be pushed to the server by typing the simple command "git push".