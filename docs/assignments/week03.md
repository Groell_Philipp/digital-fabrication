# 3. Laser cutting  

This week I learned how to use the first machine in the FabLab - the laser cutter.

## The basics 

A laser cutter cuts materials such as wood using a laser beam. When cutting something, the following parameters have to be considered: 

- Speed: the faster the laser moves the less deep a cut will be at a point
- Power: the more power is applied the deeper a cut will be
- Frequency: different materials require a different frequency of the laser beam, wood is cut with a low frequency in order not to burn during the cutting process
- Vector / raster: decides wether the material is cut or engraved 

## Designing for the laser cutter 

A laser cutter can only cut 2D objects. Therefore, any 3D object has to be assembled from 2D objects as discussed in the previous assignment. Furthermore, every object has to be in the x-y plane in order to be recognised. 
Another aspect that has to be taken care of is the kerf. It is the width of the material that is being removed while cutting. This is especially important when designing joints.
In order to make two parts actually join, a certain value either has to be added to one part or be subtracted from the other part of the joint. 
Finally, before rendering and exporting an OpenSCAD file for the laser cutter, the number of parts which shall be exported has to be specified. This is done by assigning that number to the variable "partNo". In my example the keyboard tray consists of 7 parts. 
Once that was done, I rendered and exported my project to a DXF file. DXF is a vector based format. Note: text written on an object in OpenSCAD gets lost during the export. 

## Files 

- [keyboard tray OpenSCAD file export-ready](../files/openscad/keyboard_tray_export.scad)
- [keyboard tray DXF file](../files7dxf/keyboard_tray_export.dxf)