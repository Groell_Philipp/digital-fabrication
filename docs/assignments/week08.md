# 8. Input devices 

This week I learned how to control input devices using an Arduino. 

## Definition and examples 

An input device converts a change in the physical world to a digital or an analog signal. 
Examples for input devices are human interface devices (buttons, keyboards, mouses, capacitive sensors, microphones etc.), ultra sonic sensors, light sensors and infrared sensors. 

## Assignment 

For this assignment i wrote a program which controls the sonar sensor HC-SR 04, which I am going to use in my final project. Every second it reads the distance in cm from the sensor and prints it on the serial monitor. 
As discussed [Here](https://groell_philipp.gitlab.io/digital-fabrication/assignments/week06/#connecting-the-parts), the SR 04 has a trigger and an echo pin. The sensor requires a HIGH signal at the trigger pin for at least 10 microseconds to start a measurement. 
Then, a signal is sent out by the echo pin. Using the ``` pulseIn() ``` function it is possible to measure the time th signal takes to be recognised by the sensor again. 
Finally, the distance to an object is calculated by deviding the time returned by the ``` pulseIn() ``` function by 2 to get the time the signal took for one way and deviding that by 29.1 to get the distance in centimeters. 

## Usefull links 

- [Distance measuring with an Arduino using the HC-SR 04 sonar sensor (In German)](https://www.mikrocontroller-elektronik.de/ultraschallsensor-hc-sr04/)
- [pulseIn() Arduino language reference](https://www.arduino.cc/reference/en/language/functions/advanced-io/pulsein/) 

## Files 

- [Distance measuring (Arduino source code file)](../files/Arduino/distanceMeasuring.ino)