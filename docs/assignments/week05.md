# 5. 3D printing 

This week I learned how to use the 3D printer and how to prepare 3D models for the printer. 

## Designing for the 3D printer 

Designing for a 3D printer is much easyer than designing for a laser cutter, because it is possible to use 3D objects instead of creating 3D objects out of joined 2D objects. Generally, there are two different design patterns: additive and subtractive design. 
While additive design means joining objects together, objects are subtracted from each other when using the subtractive design pattern. 
For this assignment I designed two objects: again, the keyboard tray and a holder for headphones. 
Designing for the 3D printer can take less efforts than designing 3D objects for the laser cutter. This is because for the printer it is not necessary to design each part and its joints. If you compare the keyboard tray file listed below with the one for the laser cutter, you will notice that it is a good example for what I just mencioned. 
Since the printable keyboard tray is made of 3 blocks subtracted from a bigger blog it can be done in 8 lines of code. The keyboard tray for the laser cutter has more than 80 lines. However, for this assignment I only printed the headphone holder. 

## Preparing to print 

Before being able to print the object has to be exported to an STL file which can be imported by Ultimaker Cura - a software which is used for setting some parameters for the printer. 

### Important parameters 

There are 2 very important parameters among the settings that can be made: layer width and In Fill. Both can be used to reduce the printing time. 
Layer width obviously defines the width of each layer. The thicker a layer is the less layers the printer has to print the less time the print takes. 
In order to understand In Fill it is important to know that the internal structure of a 3D printed model looks like a net. Therefore, In Fill specifys how much of this "net" is filled with material. 
For the headphone holder I set Layer Width to 0.5 cm and In Fill to 10%. 
![The gray headphone holder placed on a table](../images/week05/IMG_1555.jpg) 

## Files 

- [Keyboard tray, printable version (OpenSCAD file)](../files/openscad/keyboard_tray_printable.scad)
- [Headphone holder (OpenSCAD file)](../files/openscad/3DTest.scad)
- [Headphone holder (STL file)](../files/stl/Headphone_Holder.stl)