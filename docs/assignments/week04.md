# 4. Electronics production 

This week I learned about the Arduino board and how to program it. For this assignment I did research on the different kinds of pins and signals an Arduino board is able to handle. 

## Digital pins 

Before using a pin - no matter if it is a digital or an analog one - it has to be specified wether the pin shall be used as an input or output pin. 

Digital pins have 2 states: high (5 volts) and low (0 volts).
By default digital pins are defined as input pins. They don't demand much current and can change states very easyly. While use cases for such behaviour do exist it is important to note that input pins which are not connected to a device will change their state quite randomly, because they absorbe electrical noise from the environment. 
Output pins work kind of opositely. They provide much current to other cirquits or devices. This current can either be positive (source) or negative (sink). 

## Analog pins 

Although their main purpose is to read input data from analog sensors analog pins can function similar to digital pins. In contrast to digital pins, the signal transfered via an analog pins theoretically can be infinitely rather than being bound to 2 values. 
When using an analog pin both in digital and in analog mode it is recommended to add a short delay after switching from digitalRead to analogRead. This is because switching causes some electrical noise. 

## Pulse Width Modulation (PWM) 

PWM is a way to use digital signals to simulate analog signals. 
The basis for such analog values between 5 and 0 Volts is a sequence of "on" and "off" signals. The time a signal spends being switched to "on" is called pulse width. Manipulating the pulse width results in values between 5 and 0 Volts. 
This technique can be usefull when using an Arduino board with 8 analog and 13 digital pins and reading data from many analog sensors. 

## Pins on the ATMega328 

The essencial pins on the board are VCC and GND. VCC is the digital supply voltage and GND stands for ground. They are used for power supply of external hardware. The ATMega328 has got two of each. 

### Port B 

Port B is made out of 8 generic input/output pins. They can be used for oscillators so they support PWM. 

### Port C 

Port C is a 7-bit port (= 7 pins). Six of them can be used as generic i/o pins without support for PWM. 
The 7th pin is used to reset the board if not specified differently. 

### Port D 

Port D is another set of 8 generic i/o pins which don't support PWM. 

### AVCC 

The analog to digital converter has its own supply voltage. However, it always has to be connected to the VCC - no matter if the a/d converter is used. 

### AREF 

The AREF is the reference pin for the a/d converter. It sets the maximum voltage. 

### ADC 7 / 6 

The AD pins 6 and 7 serve as analog input pins for the analog/digital converter.