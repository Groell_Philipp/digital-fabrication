# 7. Embedded programming 

This week I learned how to program an Arduino board. 

## The basics 

Although it is possible to program an Arduino in C it also comes with its own easy to understand programming language. Every program must at least contain 2 functions: ``` setup() ``` and ``` loop() ``` . 
The setup function is executed only once when the board is powered. You can set some configurations e.g. pin configurations (more about it later). 
In the loop function you put code that is executed in a loop while the board is powered. Of course it is also possible to define custom functions outside the loop function and call them inside it. This helps to structure code. 

## Control structures 

Like other high level programming languages the Arduino programming language comes with the most commonly used control structures. 
You can use if / else statements, for and while loops and switch case statements. 

## Using pins to access external hardware 

As discussed in the previous assignments the Arduino is equipped with digital and analog pins which can be used to connect devices such as sensors, buttons, LEDs etc. 
To use a pin you first have to set the mode (input or output). This is done by using the pinMode function. It takes 2 parameters: the pin number and the mode (INPUT|OUTPUT). You usually put such statements in the setup function. 
To read a signal from a device connected via a pin use the analogRead or digitalRead function depending on which kind of pin you are using. Both functions require the pin number as a parameter. 
Writing signals is done by using either the digitalWrite or the analogWrite function. They are called by ``` digitalWrite(pinNumber,LOW|HIGH) ``` and ``` analogWrite(pinNumber,value) ``` . Value is an integer going from 0 (always off) to 255(always on). 
Note that analogWrite is not related to analog pins or the analogRead function in any way. Instead, it uses the PWM functionality. 

## Communication between a PC and an Arduino via the serial interface 

Using the serial interface it is possible to send data from an Arduino to the PC it is connected to and vice versa. 
Before data can be sent and received a connection must be established. This is done by calling the function ``` Serial.begin() ```  in the setup function. To be more precised, Serial.begin sets the data rate in bits per second (baud). Use one of the standard data rates (e.g. 9600). 
Now, data can be sent and read using the ``` Serial.write() ``` and ``` Serial.read() ``` functions. Printing something on the serial monitor is done by calling the ``` Serial.println() ``` or ``` Serial.print() ``` function. 
It is important to know that the Arduino Uno has a serial buffer of only 64 bytes. Therefore, it is usefull to clear the buffer, which is done by using the ``` Serial.flush() ``` function. The ``` Serial.available() ``` function returns the number of bytes left in the serial buffer. 

## Assignment 

For this assignment, I had to write a program which makes use of signal processing, control structures and serial communication. 
The code you can find below is supposed to run on an Arduino with a button and a speaker connected to it. Once the button is pressed the speaker is turned on and off 10 times in a loop. Before that the user has to set the delay between the turning on and off which is transfered via the srial interface. 
The Arduino returns the current status of the speaker, which is displayed by the srial monitor of the Arduino IDE. 
The following code example shows the loop function of my assignment: 

``` 
	boolean btnState = digitalRead(btnPin); // read the current button status (HIGH || LOW)
 if (btnState==LOW) { // true if the button has been pressed
  Serial.println("Please specify the delay: ");
  byte delay = Serial.read(); // reads a byte transfered via the serial connection
  Serial.flush(); // flush the serial buffer to avoid overflows
  for (int i = 0; i < 10; i++) {
    digitalWrite(speakerPin, HIGH); // turn on the speaker
    Serial.write("Speaker on, waiting"); // transcribtion of the current board status
    delay(delay); // pause the code exicution by the specified duration
    digitalWrite(speakerPin, LOW); // turn off the speaker
    Serial.write("Speaker off, waiting");
    delay(delay);
    Serial.flush();
  }
 }
 else {
  digitalWrite(speakerPin, LOW);
 }
} 
``` 

## Files 

- [Source code assignment week 07](../files/Arduino/Assignment_Week07.ino)

## Usefull links 

- [The Arduino language reference](https://www.arduino.cc/reference/en/)