# 9. Output devices 

This week I learned how to control output devices with the Arduino. 

## Definition and examples 

An output device converts a signal to a change in the real world. 
Examples are LEDs, RGB LEDs, displays, speakers, DC motors, servo motors, stepper motors and brushless motors. 
Some output devices require a driver/controler or a relay. A driver is needed in case the microcontroller is not able to output the signal the output device requires. The driver converts the microcontroler's output signal to the signal required by the device. 
A relay is an on/off switch, which receives a low electrical signal and drives a high electrical power. 

## Assignment 

For this assignment I wrote a program which controls the vibration motor, which I am going to use in my final project.
The vibration motor is a simple DC motor. I attached a 10 V transistor to it to drive the motor. 
The program receives an integer via the serial interface and takes it as the parameter for the ``` analogWrite() ``` function. 
Because it turned out that the motor is not working properly when it gets powered the first time, an incial signal is sent to the motor for 7 ms, which makes the motor operate with a high frequency before it processes the serial input. 
Here, you can see the loop function: 

``` 
void loop() {
  if (Serial.available() > 0) { // true if data is stored in the serial buffer
    intensity = Serial.parseInt(); // parse the received data to an integer
    if ((intensity > prevIntens) && (intensity > 0)) { // at first exicution prevIntens = 0, thus triggers the power up signal
      analogWrite(motorPin, 10);
      delay(6);
      // analogWrite(motorPin, 0);
    }
    analogWrite(motorPin, intensity); // vibration intensity according to the set value
    prevIntens = intensity; // update prevIntens not to fire the power up signal at next exicution of the loop function
    // delay(1000);
    // analogWrite(motorPin, 0);
    // delay(1000);
  }
  } 
  ``` 

## Files 

- [Vibration test (Arduino source code file)](../files/Arduino/vibrationTest.ino)