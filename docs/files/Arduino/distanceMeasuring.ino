byte trigger = 5;
byte echo = 9;

void setup() {
  pinMode(trigger, OUTPUT);
  pinMode(echo, INPUT);
  digitalWrite(trigger, HIGH);
  Serial.begin(9600);
  Serial.println("It's working!");
}

void loop() {
  float distance = getDistance();
  Serial.print("Object detected in ");
  Serial.print(distance);
  Serial.print(" cm");
  Serial.println();

  delay(1000);

}

float getDistance() {
  float dist = 0.00;
  long tm = 0;

  digitalWrite(trigger, LOW);
  delayMicroseconds(3);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  delayMicroseconds(10);
  tm = pulseIn(echo, HIGH);
  dist = tm * 0.034 / 2;
  return(dist);
}
