byte btnPin = 7; // button is connected at pin 7
byte speakerPin = 2; // speaker is connected at pin 2

void setup() {
  pinMode(btnPin, INPUT);
  pinMode(speakerPin, OUTPUT);
  digitalWrite(btnPin, HIGH);
  Serial.begin(9600); // establish a serial connection
  Serial.println("Hello world"); // test if the serial connection is working
  // Serial.print("Setup completed! Pin config: button at pin ");
  // Serial.print(btnPin);
  // Serial.print(" speaker at pin ");
  // Serial.print(speakerPin);
  // Serial.print(". Input buffer status: ");
  // byte bufferStatus = Serial.available();
  // Serial.print(bufferStatus);
}

void loop() {
	boolean btnState = digitalRead(btnPin); // read the current button status (HIGH || LOW)
 if (btnState==LOW) { // true if the button has been pressed
  Serial.println("Please specify the delay: ");
  byte delay = Serial.read(); // reads a byte transfered via the serial connection
  Serial.flush(); // flush the serial buffer to avoid overflows
  for (int i = 0; i < 10; i++) {
    digitalWrite(speakerPin, HIGH); // turn on the speaker
    Serial.write("Speaker on, waiting"); // transcribtion of the current board status
    delay(delay); // pause the code exicution by the specified duration
    digitalWrite(speakerPin, LOW); // turn off the speaker
    Serial.write("Speaker off, waiting");
    delay(delay);
    Serial.flush();
  }
 }
 else {
  digitalWrite(speakerPin, LOW);
 }
}
