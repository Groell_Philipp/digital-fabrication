byte motorPin = 3;
int intensity = 0;
int prevIntens = 0;

void setup() {
  pinMode(motorPin, OUTPUT);
  Serial.begin(9600);

}

void loop() {
  if (Serial.available() > 0) {
    intensity = Serial.parseInt();
    if ((intensity > prevIntens) && (intensity > 0)) {
      analogWrite(motorPin, 10);
      delay(6);
      // analogWrite(motorPin, 0);
    }
    analogWrite(motorPin, intensity);
    prevIntens = intensity;
    // delay(1000);
    // analogWrite(motorPin, 0);
    // delay(1000);
  }
  }
