/*
	main part: 595 * 120 mm
	4 x square1 30 * 18 mm
	2 x square2 120 * 18 mm
	
	joint system:
	main part: 4 x 10 mm at (1|0), (57.5|0), (1|12), (57.5|12)
	2 x 40 mm at (0|4) and (59.5|4)
	square1: 2 x 10 mm at (0|1.8) and (2|1.8)
	square2: 40 mm at (4|1.8)
	
	unions:
	main part and its joints
	square1 and its joints
	square2 and its joints
	
	intersections (?):
	4 x main part + square1 + square2
	
	modules:
	main part
	square1(rotation, translation)
	square2()
*/

// global variables
length = 595;
width = 120;
height = 18;
mWidth = 5;
hJointLength = 10;
kerf = 0.7;
partNo = 7; // parts to be exported

// modules for each component
module mainPart() {
	difference() {
		color("brown") square([length,width], center=false);
	
		// joints
		translate([10,0]) square([hJointLength-kerf,mWidth], center=false);
		translate([30,0]) square([hJointLength-kerf,mWidth], center=false);
		translate([(length-40),0]) square([hJointLength-kerf,mWidth], center=false);
		translate([(length-20),0]) square([hJointLength-kerf,mWidth], center=false);
		translate([10,(width-mWidth)]) square([hJointLength-kerf,mWidth], center=false);
		translate([30,(width-mWidth)]) square([hJointLength-kerf,mWidth], center=false);
		translate([(length-40),(width-mWidth)]) square([hJointLength-kerf,mWidth], center=false);
		translate([(length-20),(width-mWidth)]) square([hJointLength-kerf,mWidth], center=false);
		translate([0,(width/3)]) square([mWidth,(width/3)-kerf], center=false);
		translate([(length-mWidth),(width/3)]) square([mWidth,(width/3)-kerf], center=false);
	}
	
	// text
	color("yellow") translate([(length/3),(width/3)]) text("SELFMADE!", font="Braille29 DE");
}


module square1(translation, rotation) {
	translate(translation) rotate(rotation) {
		color("blue") square([50,height], center=false);
		
		// joints
		translate([10,height]) color("blue") square([hJointLength,mWidth], center=false);
		translate([30,height]) color("blue") square([hJointLength,mWidth], center=false);
	}
}

module square2(translation, rotation) {
	translate(translation) rotate(rotation) {
		color("brown") square([height,width-(2*mWidth)], center=false);
		
		// joints
		translate([height,(width/3)]) square([mWidth,(width/3)], center=false);
	}
}

// putting the parts together
mainPart();
square1([0,0,(-height)], [0,0,0]);
square1([(length-50),0,(-height)], [0,0,0]);
square1([0,width,(-height)], [0,0,0]);
square1([(length-50),width,(-height)], [0,0,0]);
square2([0,0,(-height)], [0,0,0]);
square2([length,0,(-height)], [0,0,0]);