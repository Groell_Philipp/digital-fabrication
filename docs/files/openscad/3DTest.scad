module holder() {
	difference() {
		color("brown") cube([210,210,100], center=false);
		translate([105,105,100]) color("blue") sphere(100);
		translate([105,105,100]) rotate([90,0,0]) color("blue") cylinder(110,100,100,center=false);
	}
}

holder();