/*
	2d model
*/

// the black square
color("black") square([60, 100], center=false);
// positioning the yellow circle with its center at (3|1.5)
translate([30,15]) color("yellow") circle(r=7.5);
// positioning the small black circle with the same center as the yellow one
translate([30,15]) color("black") circle(r=2);

// first line of text starts at (0.5|9)
translate([5,90]) color("yellow") {
	text("I am a 2d", size=6, font="Braille29 DE");
}

// second line of text starts at (0.5|8.5)
translate([5,85]) color("yellow") {
	text("model", size=6, font="Braille29 DE");
}